# Ecotrek Local Dev Instructions Guide

## Quick Links
* [Install dependencies](#install-dependencies)
* [Troubleshooting](#troubleshooting)
* [Important Commands in Django](#important-commands-in-Django)
* [Settings for Visual Studio Code](#settings-for-visual-studio-code)
* [Testing](#testing)
* [Git Team Rules](#git-team-rules)
  * [Our Git Workflow](#our-git-workflow)
  * [Golden Rules of Git](#golden-rules-of-git)
  * [Golden Rules of Commit](#golden-rules-of-commit)
    * [General Rules](#general-rules)
    * [Preparing to Commit](#preparing-to-commit)
    * [Formatting Style of Commit Subject and Body](#formatting-style-of-commit-subject-and-body)
    * [Reverting a Commit](#reverting-a-commit)


### Install dependencies

To set up your local environment before starting to work on the project, follow the steps:

   1. Create virtual environment venv inside local_dev_env folder

        `$ python -m venv venv`

   2. Activate venv

        * Linux and MacOs: `$ source local_dev_env/venv/bin/activate`
        * Windows: `$ local_dev_env/venv/Scripts/Activate.ps1` or cd to Scripts and enter `activate`

   3. Upgrade pip
   
        `$ python -m pip install --upgrade pip`

   4. Install requirements
   
        `$ pip install -r requirements.txt`
        
   Make sure to follow up with [Important Commands in Django](#important-commands-in-Django)
        
### Troubleshooting

   **OSError: mysql_config not found**<br>
   
   -> pip install mysqlclient does not work<br>
    Steps to fix this issue:<br>
    
   1. Install brew<br>
        `$ pip unlink mariadb` (eventhough you might not have it just to be sure)<br>
        `$ brew install mysql-connector-c`<br>
        
   2. still getting errors<br>
        `$ brew uninstall mysql-connector-c`<br>
        `$ brew install mariadb-connector-c`<br>
        `$ pip install mysqlclient`<br>
        
   3. still not working?<br>
        Continue on [this thread](https://medium.com/@MrWeeble/homebrew-on-mac-and-pythons-mysqlclient-ea44fa300e70)
    

### Important Commands in Django

   Make sure you already did the steps in [Install dependencies](#install-dependencies)

   1. Make sure you have venv activated

        * Linux and MacOs: `$ source local_dev_env/venv/bin/activate`
        * Windows: `$ local_dev_env/venv/Scripts/Activate.ps1`

   2. Migrate django project
    
       In terminal: move to app folder (location of manage.py) <br>
        Example:<br>
        `$ cd ..` (in case you need to move back)<br>
        `$ cd app`<br>
        `$ python manage.py migrate`<br>

   3. Make migrations of django project<br>
        * Move in terminal to app folder 
        * `$ python manage.py makemigrations`

   4. Collect static files of django project
        * Move in terminal to app folder
        * `$ python manage.py collectstatic`

   5. Run local server
        * `$ python manage.py runserver`
        
### Settings for Visual Studio Code
These settings are optional. Source: [Django Tutorial for VSC Code](https://code.visualstudio.com/docs/python/tutorial-django)

   1. For better code completion in Visual Studio Code go to: 
    
       --> User Settings ("Ctrl + ," or File > Preferences > Settings if available)** <br>
       
       Put in the following (please note **the curly braces** which are required for custom user settings in VSC):
       
       New Version:
       * Press "ctr+shift+P" to open the the Command Palette.
       * In command palette type Preferences: Configure Language Specific Settings.
       * Select Python. Here paste the code inside **the curly braces**.<br>
       
       Example: <br>
            `
            {
                "C_Cpp.updateChannel": "Insiders",
                 "[python]": { 
                 },
                 "python.linting.pylintArgs": [
                 "--load-plugins=pylint_django",
                    ]
            }
        `
        
   2. Access to code of django libraries

   * In VS Code, open the Command Palette (View > Command Palette or (Ctrl+Shift+P)). 
   * Then select the Python: Select Interpreter command:
   * If path of venv is not shown (or in this case local_dev_env/venv) click on "Enter interpreter path..." > Find... and select python.exe
found in venv/Scrips

### Testing
 Tests are written using the in-built Django test framework that is based on the Python unittest standard library.
 The test classes inherit from the Django base class TestCase  ([source](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Testing)).

 * How to run all the tests using the terminal
    1. move to app folder (location of manage.py)<br>
        Example:<br>
        `$ cd ..` (in case you need to move back) or<br>
        `$ cd app_folder`

    2. write command: `$ python manage.py test`

 * How to run tests in a Django app
     1. move to app folder (location of manage.py)

     2. write one of the commands:
        * To run the specified module `$ python manage.py test app_name.tests`

        * To run the specified module `$ python manage.py test app_name.tests.test_models`

        * To run the specified class `$ python manage.py test app_name.tests.test_models.YourTestClass`

        * To run the specified method `$ python manage.py test app_name.tests.test_models.YourTestClass.function_name`


[Coverage](https://coverage.readthedocs.io/en/coverage-5.2/) is a Python module that reports how much of the code has been tested. When writing tests, it is indicated to
use the report of the coverage module to identify untested code.

 * How to run coverage using the terminal
    1. if you do not have coverage installed, follow the steps at [Install dependencies](#install-dependencies)
    2. move to app folder (location of manage.py)

    3. write commands:<br>
      `$ coverage run --source='.' manage.py test (optional: myapp)`<br>
      `$ coverage report`
      

### Git Team Rules

#### Our Git Workflow
There are 2 accepted workflows:

   1. Push to master: you can push your commits directly to master. <br>
      
      Optional Steps:

        `git status` <br>
        `git add folder/file1` <br>
        `git add folder/file2` <br>
        `git commit -m "#001 Add subject line" -m "#001 Add body(optional)"` <br>
        `git pull` <br>
         TEST CODE! <br>
        `git push origin remote_master` <br>
        
        OR
        
        `git stash`<br>
        `git pull`<br>
        `git stash apply`<br>
        `git add folder/file1` <br>
        `git add folder/file2` <br>
        `git commit -m "#001 Add subject line" -m "#001 Add body(optional)"` <br>
        TEST CODE! <br>
        `git push origin remote_master` <br>
        
        [Source](https://stackoverflow.com/questions/18529206/when-do-i-need-to-do-git-pull-before-or-after-git-add-git-commit)

   2. Branching: for each task/issue you can create a branch. After finishing the task you can either merge the branch into master or merge master into your branch and then push the code to master. <br>
      
      Optional Steps:

        `git checkout branch_name`<br> 
        `git status` <br>
        `git add folder/file1` <br>
        `git add folder/file2` <br>
        `git commit -m "#001 Add subject line" -m "#001 Add body(optional)"` <br>
        `git pull` <br>
        `git push remote_branch local_branch` <br>
        `git checkout master` <br>
        `git pull` <br>
        `git merge remote_branch` <br>
         TEST CODE! <br>
        `git push remote_master local_master` <br>
        
   * In case you encounter code conflicts: <br>
      
      Optional Steps:
        
        1. If you want to return to the state of the history before merge, do `git merge –abort`. Else:
        
        2. Understand what happened: 
           > Did one of your colleagues edit the same file on the same lines as you? Did they delete a file that you modified? Did you both add a file with the same name?
             
        3. If the code of the same file was affected, look at the file 
        
        4. Communicate with the person that made changes to the same code portion.
        
        5. Clean up code. Check [here](https://opensource.com/article/20/4/git-merge-conflict) and search for other resources on how to clean up.
        
        6. `git commit -m "#001 Add subject line" -m "#001 Add body(optional)"` <br>
        
        7. TEST CODE! <br>
        
        8. Merge or Push to master (see above)
        
        9. In case you did a mistake while resolving a conflict, `git reset –hard <commit-hash>` and do again.       
   

<br>
There is also a blessed repository available. The master branch is weekly merged into the blessed repository by assigned developers. No one pushes their code after finishing an issue to the blessed repository.
<br>
Everybody is trusted to follow the rules below and to make sure that their push will not break the code. If the code breaks, you are responsible for the merge hell and for cleaning up.

#### Golden Rules of Git
1. Before starting to work and as often as possible, do a pull request.
2. Before pushing code/merging/rebasing, **always** review the changed files.
3. For those working with branches: create a new branch for each Gitlab issue. Delete the branch after one or a few days.
4. Consider rebase instead of merge. [Why?](https://kalamuna.atlassian.net/wiki/spaces/KALA/pages/1540298/Git+Workflow)

#### Golden Rules of Commit

##### General Rules

1. An issue on Gitlab can have multiple tasks, and a commit represents the code changes for either a task or an issue.
2. Commit only related changes. The commit should represent a stand-alone unit. Before committing, ask the question:
 If cherry-picked/reverted would the commit make sense or would the code break?
3. Frequent smaller commits vs. one giant commit. Tip: you can commit the code and keep on working before making a push.
<br><br>
##### Preparing to Commit

Optional Steps:

  1. `git pull` <br>
    
       If the pull fails because the files you edited overlap with your colleague's latest commit(s):
        
        a. Understand what happened: 
        
            > Did one of your colleagues edit the same file on the same lines as you? Did they delete a file that you modified? Did you both add a file with the same name?
                         
        b. If the code of the same file was affected, look at the file 
                    
        c. Communicate with the person that made changes to the same code portion.
                    
        d. Clean up code. Check [here](https://opensource.com/article/20/4/git-merge-conflict) and search for other resources on how to clean up.
            
  2. `git commit -m "#001 Add subject line" -m "#001 Add body(optional)"` <br>

  3. Optional: Push up your changes.
<br><br>

##### Formatting Style of Commit Subject and Body

1. Add a subject line with `-m` tag. Limit it to 50 characters.
2. Capitalize the subject line : **Merge branch 'feature1'** NOT  *merge branch 'feature1'*
3. Do not end the subject line with a period
4. Use the imperative mood in the subject line: **Remove deprecated methods**, **Release version 1.0.0** NOT 
*Fixed bug with Y* or *Changing behavior of X* . 
The subject line should complete the sentence *If applied, the commit will..*.
5. If applicable, add a body with another `-m` tag. Wrap the body at 72 characters. Use the body to explain WHY the commit was made.
6. Include an Issue ID in every branch name and every commit message (e.g., `git commit -m "#125: Remove cat gifs"`).


Standard structure of git commit: `git commit -m "#001 Add subject line" -m "#001 Add body"`

Examples of good git commits:

`git commit -m "#002: Fix CompositePropertySource" -m "The program crashed when ____.\n \n Users were not able to install the application.\n \n
The application can now output verbose information.\n
Pages were loading too slowly (>150ms)."` 

`git commit -m "#003: Fix new file bug -m "#003: When a user selects File -> New, the application crashes before the old document could be saved."`


Resources for more examples of good commits: 
[1](https://medium.com/@joshuatauberer/write-joyous-git-commit-messages-2f98891114c4),
[2](https://gist.github.com/turbo/efb8d57c145e00dc38907f9526b60f17)
<br><br>

##### Reverting a Commit

The instructions differ due to the editor you use or if you use Git Bash. But the basics are:

You want to nuke commit C and never see it again and lose all the changes in locally modified files?
    `git reset --hard HEAD~1`
    
You want to undo the commit but keep your changes for a bit of editing before you do a better commit?
   `git reset HEAD~1`
   
You want to undo your commit but leave your files and your index.
    `git reset --soft HEAD~1`
    
[Source](https://stackoverflow.com/questions/927358/how-do-i-undo-the-most-recent-local-commits-in-git)