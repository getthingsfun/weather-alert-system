Django==3.0.7
python-dotenv==0.14.0
# gunicorn==20.0.4
# psycopg2-binary==2.8.5

pylint==2.5.2
pylint-django

# Import-Export Data
# django-import-export==2.3.0

# # mysqlclient==1.4.6

# # test
# # uwsgi>=2.0,<2.1
# # dj-database-url>=0.5,<0.6
# # psycopg2>=2.8,<2.9

# # Datetime
# python-dateutil==2.8.1

# # String searching and replacing
# regex==2020.7.14

# # Coverage of Tested Code
# coverage==5.2